var Forms_Loaded = 'y',
	Forms_Timeout,
	Floc = $ENV['lng']['loc'],
	Flng = $ENV['lng']['lng'],
	$FRM = {};
	
(function(){
	load_CSS('lib/Forms');
	if($PGE && $PGE['loc']) Floc = $PGE['loc'];
	if($PGE && $PGE['lng']) Flng = $PGE['lng'];
	$ICO['switch'] = '<svg viewBox="0 0 99 73"><path opacity=".4" d="M36 73h29c44-2 47-70-1-73H36c42 2 47 68 0 73z"/><circle class="SwitchSVG C4fl" cx="31.5" cy="36.5" r="31.5"/></svg>';
})();

function SpokeForm(tar, $f){
	var e = getElem(tar);
	if(e && $f){
		var step = 0,
			loc = '',
			lng = $ENV['lng']['lng'],
			hasloc = ($f['fld'][step]['country']) ? 'y' : 'n',
			hasadr = ($f['fld'][step]['address']) ? 'y' : 'n',
			ins = '';

		$FRM[tar] = $f;
		$FRM[tar]['step'] = step;

		for(var k in $FRM[tar]['fld'][step]){
			var v = $FRM[tar]['fld'][step][k],
				id = tar+'_'+k,
				req = (v['req'] && v['req'] === 'y') ? '<b class="C4">*</b> ' : '',
				reqclss = (v['req'] && v['req'] === 'y') ? ' ReqField' : '',
				plh = (v['plh']) ? ' placeholder="'+v['plh']+'"' : '';
	
			ins += '<div class="SpokeField'+reqclss+'">';

			if(k === 'address'){
				ins += '<div id="'+id+'"></div>';
			}else{
				ins += '<label>'+req+v['lbl']+'</lable>';

				if(k === 'country'){
					ins += '<div id="'+id+'" class="ComboBox left"></div>';
				}else if(k === 'msg'){
					ins += '<textarea id="'+id+'" class="TextareaExpand"'+plh+'></textarea>';
				}else{
					var typ = 'text';
					if(k === 'upload') typ = 'file';
					ins += '<input id="'+id+'" type="'+typ+'"'+plh+' />';
				}
			}
			ins += '</div>';
		}

		ins += '<div class="SpokeFormSubmit shim10">'+
			'<div class="SpokeFormBtn BtnSubmit BtnSubmitInactive">'+$f['btn']+'</div>'+
			'<div class="SpokeFormMsg txt_smalltext C2">'+$LNG['ui'][lng]['frm']['intro']+'</div>'+
		'</div>';

		e.classList.add('SpokeForm');
		setAttr(e, {"data-step":step});
		e.innerHTML = ins;
	
		TextareaExpand_init(e);

		if(hasloc === 'y'){
			var defloc = $f['fld'][step]['country']['def'];
			if(defloc) loc = defloc;
			load_Countries(lng, function(){
				$CTY = {};
				for(var k in $LOC){
					$CTY[k] = {'lbl':$LOC[k]['n']};
					if($LOC[k]['a']) $CTY[k]['alt'] = $LOC[k]['a'];
				}
				ComboBox(tar+'_country', loc, 'select', $CTY);
				//getElem(tar+'_country_fld').focus();
			});
		}
		if(hasadr === 'y' && loc){
			Address_init(tar, loc, lng, tar+'_address', function(){

			});
		}
	}
}

function SpokeForm_Update(e){
	var f = getClosest(e, '.SpokeForm'),
		p = e.closest('.SpokeField'),
		req = 'n';

	if(f && p){
		//make sure it's a SpokeForm event and not general usage
		if(p.classList.contains('ReqField')) req = 'y';

		var tar = f.getAttribute('id'),
			step = f.getAttribute('data-step'),
			fld = e.getAttribute('id').replace(tar+'_', ''),
			val = e.value;

		if(isObj($FRM[tar]) > 0){
			$FRM[tar]['fld'][step][fld]['val'] = val;
		}
		SpokeForm_Check(f);
	}
}

function SpokeForm_Check(f){
	var tar = f.getAttribute('id'),
		step = f.getAttribute('data-step'),
		$f = $FRM[tar]['fld'][step];

	if(isObj($f) > 0){
		console.log($f);
		var reqpass = 'y',
			btn = f.querySelector('.SpokeFormBtn');

		for(var k in $f){
			if($f[k]['req'] && $f[k]['req'] === 'y'){
				var fld = getElem(tar+'_'+k),
					val = fld.value;

				if(val){

				}else{
					reqpass = 'n';
				}
			}
		}
		if(reqpass === 'y'){
			btn.classList.remove('BtnSubmitInactive');
		}else{
			btn.classList.add('BtnSubmitInactive');
		}
	}
}

function SpokeForm_Submit(e){
	var f = getClosest(e, '.SpokeForm'),
		tar = f.getAttribute('id');

	if($FRM[tar]){
		console.log($FRM[tar]);

		var proctext = e.textContent || e.innerText,
			msg = f.querySelector('.SpokeFormMsg');

		if($FRM[tar]['btn_sending']) proctext = $FRM[tar]['btn_sending'];
		e.classList.add('BtnSubmitActive');
		e.innerHTML = '<div class="BtnSubmitIco C1fl"><div class="spin">'+$ICO['spoke']+'</div></div>'+proctext;
		msg.classList.add('hide');

		var $d = {},
			qry = '';

		for(var step in $FRM[tar]['fld']){
			for(var k in $FRM[tar]['fld'][step]){
				var v = $FRM[tar]['fld'][step][k];
				$d[k] = (v['val']) ? v['val'] : '';
			}
		}
		if($FRM[tar]['mde'] === 'contact'){
			qry = 'manage_Message';
			$d['loc'] = $PGE['loc'];
			$d['lng'] = $PGE['lng'];
		}else if($FRM[tar]['mde'] === 'contribute' && $FRM[tar]['tbl']){
			$d['tbl'] = $FRM[tar]['tbl'];
			qry = 'manage_Contribute';
		}
		if(qry && $d){
			console.log($d);
			get_Data(qry, '', $d, function(dta){
				console.log(dta);
				if(dta && dta['msg'] === 'OK'){
					f.innerHTML = '<div class="shim20 LR80 txt_smalltext C3">'+$FRM[tar]['rsp']+'</div>';
				}else{
					Error('Message', dta);
				}
			});
		}
	}
}

function ComboBox(tar, val, mde, $SEL){
	var cb = getElem(tar);
	if(cb && $SEL){
		cb.innerHTML = '<div id="'+tar+'_btn" class="ComboBoxBtn C1bk C2bk_hov C0fl"><div class="rot90">'+$ICO['arrow']+'</div></div>'+
			'<div class="ComboBoxFldOuter">'+
				'<div class="Ico24 ComboBoxFldIco">'+$ICO['magnify']+'</div>'+
				'<input id="'+tar+'_fld" type="text" autocomplete="off" class="ComboBoxFld C1br" />'+
			'</div>'+
			'<div id="'+tar+'_sel" class="ComboBoxSelect C1br hide"></div>';

		var fld = getElem(tar+'_fld'),
			sel = getElem(tar+'_sel');

		if(val){
			if($SEL[val]){
				fld.value = $SEL[val]['lbl'];
				cb.querySelector('.ComboBoxFldOuter').classList.add('WidgetteOn');
				setAttr(cb, {"data-val":val});
			}else{
				fld.value = val;
			}
		}

		var evt_combo_change = function(f){
				ComboBox_Update(tar, f.target.value, $SEL);
			},
			evt_combo_pointerup = function(f){
				var e = f.target,
					c = e.classList;
				
				if(c.contains('ComboBoxFld')){
					e.setSelectionRange(0, e.value.length);
				}else if(c.contains('ComboBoxBtn')){
					if(sel.classList.contains('hide')){
						ComboBox_Options(sel, '', $SEL);
					}else{
						ComboBox_Toggle(sel, 'close');
					}
				}else if(c.contains('ComboBoxOption')){
					ComboBox_Update(tar, e.getAttribute('data-val'), $SEL);
				}
			},
			evt_combo_keyup = function(f){
				var e = f.target,
					val = e.value;
			
				if(f.keyCode === 13){
					var val_key = 'n';
					if(val){
						for(var k in $SEL){
							val_key = ComboBox_Match(val, k, $SEL);
							if(val_key !== 'n') break;
						}
					}
					console.log(val_key);
					if(val_key !== 'n') ComboBox_Update(tar, val_key, $SEL);
				}else if(f.keyCode === 27){
					ComboBox_Toggle(sel, 'close');
					e.value = '';
				}else if(val){
					clearTimeout(Forms_Timeout);
					Forms_Timeout = setTimeout(function(){
						ComboBox_Options(sel, val, $SEL);
					}, 250);
				}else{
					ComboBox_Toggle(sel, 'close');
				}
			};

		//fld.addEventListener('change', evt_combo_change);
		fld.addEventListener('keyup', evt_combo_keyup);
		cb.addEventListener('pointerup', evt_combo_pointerup);
	}
}

function ComboBox_Input(e, mde){
	var p = e.parentNode,
		fld = p.querySelector('.ComboBoxFld');

	if(mde === 'display'){
		fld.classList.add('hide');
	}else{
		fld.classList.remove('hide');
		fld.focus();
	}
}

function ComboBox_Toggle(e, mde){
	var b = e.parentNode.querySelector('.ComboBoxBtn');
	if(mde === 'open'){
		e.classList.remove('hide');
		b.innerHTML = '<div class="rot45">'+$ICO['plus']+'</div>';
	}else{
		e.classList.add('hide');
		b.innerHTML = '<div class="rot90">'+$ICO['arrow']+'</div>';
	}
}

function ComboBox_Match(val, key, $d){
	var match = 'n';

	if(val && $d && $d[key]){
		var vl = val.toLowerCase(),
			ky = key.toLowerCase(),
			lb = ($d[key]['lbl']) ? $d[key]['lbl'].toLowerCase() : '';

		if(vl === ky || vl === lb){
			match = key;
		}else if(lb.indexOf(' ') > -1){
			var $l = lb.split(' '),
				llen = $l.length;

			for(var i = 0; i < llen; i++){
				var nme = $l[i];
				if(vl === nme || nme.lastIndexOf(vl, 0) === 0){
					match = key;
					break;
				}
			}

		}else if(lb.lastIndexOf(vl, 0) === 0){
			match = key;
		}else if($d[key]['alt']){
			var alen = $d[key]['alt'].length;
			for(var i = 0; i < alen; i++){
				var alt = $d[key]['alt'][i].toLowerCase();
				if(vl === alt || alt.lastIndexOf(vl, 0) === 0){
					match = key;
					break;
				}
			}
		}
	}
	return match;
}

function ComboBox_Options(e, val, $SEL){
	var $R = {},
		ins = '';

	for(var k in $SEL){
		if(val){
			var val_key = ComboBox_Match(val, k, $SEL);
			if(val_key !== 'n') $R[k] = $SEL[k];
		}else{
			$R[k] = $SEL[k];
		}
	}

	if(isObj($R) > 0){
		for(var k in $R){
			ins += '<div class="ComboBoxOption" data-val="'+k+'">'+$R[k]['lbl']+'</div>';
		}
	}else{
		ins += '<div class="center shim20">No Results</div>';
	}
	ComboBox_Toggle(e, 'open');
	e.innerHTML = ins;

}

function ComboBox_Update(tar, val, $SEL){
	var e = getElem(tar),
		fld = getElem(tar+'_fld');

	if(e && fld && val && $SEL[val]){
		setAttr(e, {'data-val':val});
		fld.value = $SEL[val]['lbl'];

		var fnc = e.getAttribute('data-fnc');
		if(fnc && typeof window[fnc] == 'function'){
			window[fnc](val);
		}else{
			var evt = new Event('change');
			fld.dispatchEvent(evt);
		}

		ComboBox_Toggle(getElem(tar+'_sel'), 'close');
	}
}

function LocLngFld(loc, loctar, lng, lngtar){
	var r = {};
	if(loctar) r['loc'] = LocLang_LocFld(loc, lng, loctar);
	if(lngtar) r['lng'] = LocLang_LngFld(loc, lng, lngtar);
	return r;
}
function LocLang_FldSetup(e){
	var abbr = e.getAttribute('data-abbr'),
		mde = e.getAttribute('data-mde'),
		typ = e.getAttribute('data-typ');

	abbr = (abbr && abbr === 'n') ? 'n' : 'y';	
	mde = (mde && mde === 'sys') ? 'sys' : 'app';
	typ = (typ && typ === 'loose') ? 'loose' : 'strict';

	var	$R = {'mde':mde, 'abbr':abbr, 'typ':typ};
	$R['data'] = (mde && mde === 'sys') ? $LOC : $APP['loclang'];

	return $R;
}
function LocLang_LocFld(loc, lng, tar){
	var e = getElem(tar);
	if(e){
		var $F = LocLang_FldSetup(e),
			has_loc = '',
			ins = '<option value=""> -- </option>',
			alphamde = ($F['abbr'] === 'n') ? 'a' : '',
			$C = SortObject($F['data'], 'n', alphamde),
			loccnt = $C.length;

		for(var i = 0; i < loccnt; i++){
			var k = $C[i]['key'],
				lbl = ($F['abbr'] === 'n') ? $C[i]['nme'] : k;

			if(k === loc) has_loc = k;
			ins += '<option value="'+k+'">'+lbl+'</option>';
		}

		e.innerHTML = ins;
		if(has_loc) e.value = has_loc;

		return has_loc;
	}
}
function LocLang_LngFld(loc, lng, tar){
	console.log(loc);
	console.log(lng);
	console.log(tar);
	var e = getElem(tar);
	if(e){
		var $F = LocLang_FldSetup(e),
			$L = [],
			def_lng = ($F['typ'] === 'loose' && lng) ? lng : '',
			has_lng = '',
			ins = '<option value=""> -- </option>',
			ins_pre = '<option value="'+def_lng+'">'+def_lng+'</option>';

		console.log($F);	

		e.classList.remove('o5');
		if($F['mde'] === 'app'){
			if(loc){
				for(var l in $F['data'][loc]){
					$L.push(l);
				}
			}else{
				for(var loc in $F['data']){
					for(var l in $F['data'][loc]){
						$L.push(l);
					}
				}
			}
		}else{
			if(loc){
				for(var l in $F['data'][loc]['l']){
					$L.push(l);
				}
			}else{
				for(var loc in $F['data']){
					for(var l in $F['data'][loc]['l']){
						$L.push(l);
					}
				}
			}
		}

		var lngcnt = $L.length;
		for(var i = 0; i < lngcnt; i++){
			var c = $L[i],
				lbl = c;

			if($F['abbr'] === 'n' && $LNG['sys'] && $LNG['sys'][c]){
				lbl = $LNG['sys'][c]['l'];
			}

			ins += '<option value="'+c+'">'+lbl+'</option>';

			if(def_lng && c === def_lng){
				//loose lang doesn't need to be added
				ins_pre = '';
			}
			if(c === lng) has_lng = c;
		}

		if(ins_pre != '') has_lng = lng;

		e.innerHTML = ins_pre+ins;

		if(has_lng){
			e.value = has_lng;
		}

		if(has_lng !== lng){
			var evt = new Event('change');
			e.dispatchEvent(evt);
		}
		return has_lng;
	}
}

function Address_init(pre, loc, lng, tar, callback){
	var e = getElem(tar),
		has_lng = 'n';

	if(e){
		if(loc){
			e.innerHTML = '<div id="'+tar+'_AddrGrp" class="AddressGroup">'+
				'<div class="center shim30"><div class="Ico32 C2fl"><div class="spin">'+$ICO['spoke']+'</div></div></div>'+
			'</div>';

			if(lng){
				for(var c in $APP['loclang']){
					for(var l in $APP['loclang'][c]){
						if(l === lng){
							has_lng = 'y';
							break;
						}
					}
				}
			}
			if(has_lng === 'n') lng = 'en';
			load_LanguageLib(lng, 'ui', function(){
				load_Country(loc, lng, function(){
					Address_markup(pre, loc, lng, tar);
					callback();
				});
			});
		}else{
			e.innerHTML = '<div id="'+tar+'_AddrGrp" class="AddressGroup"></div>';
		}
	}
}

function Address_markup(pre, loc, lng, tar){
	var e = getElem(tar+'_AddrGrp'),
		$J = ($LOC[loc] && $LOC[loc]['json']) ? $LOC[loc]['json'] : {},
		cline = '##t## ##p##',
		ins = '',
		AdrHTML = '<label for="'+pre+'_address1">'+$LNG['ui'][lng]['fld']['address']+'</label>'+
			'<div class="Addr1Grp">'+
				'<input type="text" id="'+pre+'_address1" />'+
				'<div class="Btn32 C1bk C2bk_hov C0fl Addr1Btn" data-id="'+tar+'_addr2grp">'+$ICO['plus']+'</div>'+
			'</div>'+
			'<div id="'+tar+'_addr2grp" class="Addr2Grp hide">'+
				'<label for="'+pre+'_address2">'+$LNG['ui'][lng]['fld']['addradd']+'</label>'+
				'<input type="text" id="'+pre+'_address2" placeholder="'+$LNG['ui'][lng]['fld']['addrtwo']+'" />'+
			'</div>',
		CtyHTML = '<label for="'+pre+'_city">'+$LNG['ui'][lng]['fld']['city']+'</label><input type="text" id="'+pre+'_city" />';

	if(loc !== 'HU') ins += AdrHTML;

	if($J && $J['postal']){
		if($J['postal']['cityline']) cline = $J['postal']['cityline'];

		var cline_len = cline.length,
			has_state = ($J['prov']) ? 'y' : 'n',
			lbl_state = 'Province',
			lbl_postal = $LNG['ui'][lng]['fld']['postal'];

		ins += '<div class="AddressCityLine">';
		for(var i = 0; i < cline_len; i++){
			if(cline[i] === 't'){
				var cls = (has_state === 'y') ? 'Col2' : 'Col4-3';
				ins += '<div class="'+cls+'">'+CtyHTML+'</div>';
			}else if((cline[i] === 's' || cline[i] === 'S') && has_state === 'y'){
				ins += '<div class="Col4"><label for="'+pre+'_state">'+lbl_state+'</label><select id="'+pre+'_state"></select></div>';
			}else if(cline[i] === 'p'){
				ins += '<div class="Col4"><label for="'+pre+'_postal">'+lbl_postal+'</label><input type="text" id="'+pre+'_postal" class="FormsPostal" /></div>';
			}
		}
		ins += '</div>';
	}else{
		ins += CtyHTML;
	}

	if(loc === 'HU') ins += AdrHTML;
	e.innerHTML = ins;

	if($J['postal'] && $J['postal']['one']) setVal(pre+'_postal', $J['postal']['one']);

	if(has_state === 'y'){
		State_field(loc, lng, pre+'_state');
	}
}

function State_field(loc, lng, tar){
	var e = getElem(tar);
	if(e){
		if(loc && $LOC[loc]['json'] && isObj($LOC[loc]['json']['prov']) > 0){
			var ins = '';
			for(var k in $LOC[loc]['json']['prov']['list']){
				var v = $LOC[loc]['json']['prov']['list'][k],
					lbl = '',
					i = 0;
				
				if(isObj(v) > 0){
					for(var l in v){
						if(i === 0) lbl = v[l]['n'];
						if(l === lng){
							lbl = v[l]['n'];
							break;
						}
						i++;
					}
				}else if(v){
					lbl = v;
				}
				ins += '<option value="'+k+'">'+lbl+'</option>';
			}
			e.innerHTML = ins;
		}
	}
}

function prepPostal(v, callback){
	v = v.toUpperCase();
	var $R = {
			'msg':'Untested',
			'val':v
		},
		regex = '',
		mask = '';
	
	if(v){
		load_Country(Floc, Flng, function(){
			if($LOC[Floc] && $LOC[Floc]['json'] && $LOC[Floc]['json']['postal']){
				var $P = $LOC[Floc]['json']['postal'];

				if($P['reg']) regex = $P['reg'];
				mask = ($P['frm']) ? $P['frm'] : 'a';
			}

			if(regex){
				var $m = v.match(regex);
				if($m){
					var mlen = $m.length,
						i = 0,
						$pos = ['a', 'b', 'c', 'd'];

					for(i = 0; i < mlen; i++){
						if(mask.indexOf($pos[i]) >= 0){
							mask = mask.replace($pos[i], $m[i]);
						}
					}
					$R['msg'] = 'OK';
					$R['val'] = mask;
				}else{
					$R['msg'] = 'Error';
				}
			}
			callback($R);
		});
	}else{
		callback($R);
	}
}

function TextareaExpand_init(p){
	p = p || document;
	var e = p.querySelectorAll('.TextareaExpand'),
		elen = e.length;

	for(var i = 0; i < elen; i++){
		TextareaExpand(e[i]);
	}
}

function TextareaExpand(e){
	e.style.height = '32px';
	var dmx = parseInt(e.getAttribute('data-max')),
		max = (dmx > 0) ? dmx : 999,
		c = window.getComputedStyle(e),
		h = parseInt(c.getPropertyValue('border-top-width'), 10)
			+ parseInt(c.getPropertyValue('padding-top'), 10)
			+ e.scrollHeight
			+ parseInt(c.getPropertyValue('padding-bottom'), 10)
			+ parseInt(c.getPropertyValue('border-bottom-width'), 10);

	if(e.value != ''){
		h = h - 13;
		h = (h > max) ? max : h;
		e.style.height = h+'px';
	}
}

function Switch(val, tar){
	var e = getElem(tar);
	if(e){
		var st = 'F',
			cls = 'SwitchSVG';

		if(val === 'T' || val == '1'){
			st = 'T';
		}else if(val === 'tog'){
			var sv = e.getAttribute('data-switch');
			if(sv === 'T' || sv == '1'){
				st = 'F';
			}else{
				st = 'T';
			}
		}
	
		if(st === 'T'){
			e.classList.add('rot180');
			setAttr(e, {"data-switch":"T"});
			cls += ' C5fl';
		}else{
			e.classList.remove('rot180');
			setAttr(e, {"data-switch":"F"});
			cls += ' C4fl';
		}
		setAttr(e.querySelector('.SwitchSVG'), {"class":cls});
	}
}

function SortObject($d, col, mde){
	var r = [];
	for(var k in $d){
		r.push({'key':k, 'nme':$d[k][col]});
	}
	if(mde === 'a' || mde === 'd'){
		r.sort(function(a, b){
			return a.nme.toLowerCase().localeCompare(b.nme.toLowerCase());
		});
		if(mde === 'd') r.sort().reverse();
	}
	return r;
}

document.addEventListener('input', f => {
	var e = f.target;
	if(e.classList.contains('TextareaExpand')){
		TextareaExpand(e);
	}
	clearTimeout(Forms_Timeout);
	Forms_Timeout = setTimeout(function(){
		SpokeForm_Update(e);
	}, 250);
});
document.addEventListener('change', f => {
	var e = f.target,
		c = e.classList,
		val = e.value;

	if(c.contains('LocLngFld')){
		var e_lng = e.getAttribute('data-lng');
		if(e_lng){
			//Locale Change
			e_lng = getElem(e_lng);
			LocLang_LngFld(e.value, e_lng.value, e_lng);
		}else{
			//Language Change
		}
	}else if(c.contains('FormsFilename')){
		e.value = CreateFilename(val);
	}else if(c.contains('FormsPostal')){
		prepPostal(val, function(d){
			var l = document.querySelector('label[for='+e.getAttribute('id')+']');
			if(d['msg'] === 'OK'){
				e.classList.remove('C4', 'C4br');
				if(l) l.classList.remove('C4');
			}else if(d['msg'] === 'Error'){
				e.classList.add('C4', 'C4br');
				if(l) l.classList.add('C4');
			}
			e.value = d['val'];
		});
	}
	SpokeForm_Update(e);
});

document.body.addEventListener('pointerup', f => {
	var e = f.target,
		id = e.getAttribute('id'),
		c = e.classList;

	if(c.contains('SpokeFormBtn')){
		SpokeForm_Submit(e);
	}else if(c.contains('Switch')){
		Switch('tog', e);
	}else if(c.contains('ComboBoxFld_Open')){
		console.log('forms_triggered');
		ComboBox_Input(e, 'input');
	}else if(c.contains('Addr1Btn')){
		var adid = e.getAttribute('data-id'),
			g = getElem(adid);

		if(g.classList.contains('hide')){
			e.innerHTML = '<div class="rot45">'+$ICO['plus']+'</div>';
			g.classList.remove('hide');
		}else{
			e.innerHTML = $ICO['plus'];
			g.classList.add('hide');
		}
	}
});