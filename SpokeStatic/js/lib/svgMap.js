var svgMap_Loaded = 'y',
	MapTooltip,
	MapZoomWheel = 0,
	MapZoomWheelTimeout,
	$svgMap = {
		'box':{
			'w':0,
			'h':0
		},
		'map':{
			'x':999,
			'y':757,
			'w':0,
			'h':0
		},
		'svg':{
			'w':999,
			'h':757,
			'r':.757757
		},
		'z':{
			'min':1,
			'max':40,
			'now':1
		}
	};

//918 .9189

(function(){
	load_CSS('lib/svgMap');
})();

function svgMap_init(tar, lng, mde, callback){
	var e = getElem(tar);
	if(e){
		e.innerHTML = '<div id="MapLoader" class="o6"><div class="Ico24 C2fl"><div class="spin">'+$ICO['spoke']+'</div></div></div>';
		load_Countries(lng, function(){
			load_Scripts(['js/lib/SpokeUI'], function(){
				load_JSON($ENV['cdn']+'json/lib/svgMap/svgMap-mercator-lowres-world'+$ENV['min']+'.json'+$ENV['cache'], function(d){
					if(d){
						var dta = JSON.parse(d);
						if(dta){
							var ins = '<div id="svgMapInner" class="Dragger"></div>'+
								'<div id="MapToolTip" class="hide noselect txt_smalltext"></div>';

							if(mde === 'full'){
								ins += '<div id="BtnZoomIn" class="C1bk C2bk_hov C0 Shadow noselect">+</div>'+
									'<div id="BtnZoomOut" class="C1bk C2bk_hov C0 Shadow noselect">–</div>';
							}
			
							e.innerHTML = ins;
			
							MapTooltip = getElem('MapToolTip');
							svgMap_size(e);
			
							var equator = MapPlotLatLng(0, 0),
								Cancer = MapPlotLatLng(-23.43652, 0),
								Capricorn = MapPlotLatLng(23.43652, 0),
								ins = '<line class="MapTropics" x1="-250" y1="'+Cancer['y']+'" x2="'+($svgMap['svg']['w'] + 250)+'" y2="'+Cancer['y']+'" />'+
								'<line id="MapEquator" x1="-250" y1="'+equator['y']+'" x2="'+($svgMap['svg']['w'] + 250)+'" y2="'+equator['y']+'" />'+
								'<line class="MapTropics" x1="-250" y1="'+Capricorn['y']+'" x2="'+($svgMap['svg']['w'] + 250)+'" y2="'+Capricorn['y']+'" />'+
								'<line class="MapTropics" x1="'+equator['x']+'" y1="117" x2="'+equator['x']+'" y2="'+$svgMap['svg']['h']+'" />';
			
							for(var c in dta){
								if($LOC[c]){
									if(dta[c]['z']) $LOC[c]['z'] = dta[c]['z'];
									if(dta[c]['c']) $LOC[c]['c'] = dta[c]['c'];
								}
								ins += '<path id="'+c+'" class="svgMap_Land" data-r="'+dta[c]['r']+'" data-z="'+dta[c]['z']+'" d="'+dta[c]['m']+'"/>';
							}
			
							ins += '<path id="ArticLine" d="M503.4 114.07l-8.37.02c-95.93 0-188.48-3.12-272.03-9.01l.22-.52c82 5.78 174.28 9.03 271.8 9.03l8.1-.01.28.5zM151.29 98.6C92.97 92.97 41.75 85.97 0 77.9v.52c18.14 3.5 38.21 6.82 60.15 9.95 28.15 4.02 58.59 7.6 90.83 10.7l.3-.5zm616.97 5.86l.46.5c58.9-4.18 113.3-9.73 161.2-16.57 25.97-3.7 49.33-7.68 69.96-11.9v-.52c-58.94 12.05-138.63 21.9-231.62 28.5zm-19.17 1.3a3867.67 3867.67 0 01-206.69 7.58l.17.49c71.96-.78 141.43-3.33 205.94-7.5l.58-.56z"/>';
							
							var mapinner = getElem('svgMapInner');
							mapinner.innerHTML = '<svg viewBox="0 0 '+$svgMap['svg']['w']+' '+$svgMap['svg']['h']+'">'+ins+'</svg>';
			
							mapinner.addEventListener('wheel', evt_map_wheel);
							e.addEventListener('pointerup', evt_map_pointerup);
							mapinner.addEventListener('pointermove', evt_map_pointermove);
							mapinner.addEventListener('pointerdown', DraggerPress);
							callback();
						}
					}
				});
			});
		});
	}
}

function svgMap_size(e){
	var $B = e.getBoundingClientRect();

	//$svgMap['w'] = VPortW;
	//$svgMap['h'] = $svgMap['w'] * $svgMap['rat'];


	$svgMap['box']['w'] = $B['width'];
	$svgMap['box']['h'] = $B['height'];

	console.log($B);
	console.log($svgMap['z']['now']);

	$svgMap['map']['w'] = $B['width'] * $svgMap['z']['now'];
	$svgMap['map']['h'] = $B['width'] * $svgMap['svg']['r'] * $svgMap['z']['now'];

	var mapinner = getElem('svgMapInner');
	mapinner.style.width = $svgMap['map']['w']+'px';
	mapinner.style.height = $svgMap['map']['h']+'px';
}

function MapZoom(z, mde){
	var newZ = z,
		oldZ = $svgMap['z']['now'],
		xDiff = 0,
		yDiff = 0,
		newW = 0,
		newH = 0,
		newX = 0,
		newY = 0;

	if(newZ > oldZ){
		if(newZ > $svgMap['z']['max']) newZ = $svgMap['z']['max'];
	}else{
		if(newZ < $svgMap['z']['min']) newZ = $svgMap['z']['min'];
	}

	newW = newZ * $svgMap['box']['w'];
	newH = newZ * $svgMap['box']['h'];

	xDiff = ($svgMap['map']['w'] - newW) / 2;
	yDiff = ($svgMap['map']['h'] - newH) / 2;
	
	console.log(newZ+' Z '+oldZ);
	console.log('Diff: '+xDiff+' '+yDiff);

	//var XCenter = $svgMap['box']['w'] / 2,
	//	YCenter = $svgMap['box']['h'] / 2;

	//if(mde === 'wheel'){
		//center to mouse
	//	XCenter = mouseX;
	//	YCenter = mouseY;
	//}

	//console.log('W/H: '+newW+' x '+newH);
	//console.log('Center: '+XCenter+' x '+YCenter);
	
	//var cpX = XCenter / $svgMap['box']['w'],
	//	cpY = YCenter / $svgMap['box']['h'];

	//console.log('Cp: '+cpX+' x '+cpY);

	newX = $svgMap['map']['x'] + xDiff;
	newY = $svgMap['map']['y'] + yDiff;

	console.log('new: '+newX+' x '+newY);

	$svgMap['map']['w'] = newW;
	$svgMap['map']['h'] = newH;
	$svgMap['map']['x'] = newX;
	$svgMap['map']['y'] = newY;
	console.log('ZOOM');
	console.log($svgMap['map']);
	$svgMap['z']['now'] = newZ;

	getElem('svgMapInner').style.cssText = 'width:'+newW+'px;height:'+newH+'px;margin-left:'+newX+'px;margin-top:'+newY+'px';
}

function MapPan(d, v){
	console.log(d);
	console.log(v);

	var mapinner = getElem('svgMapInner'),
		v = (v) ? parseInt(v) : 0, 
		newX = $svgMap['map']['x'],
		newY = $svgMap['map']['y'];

	if(d === 'U'){
		newY = newY + v;
	}else if(d === 'D'){
		newY = newY - v;
	}else if(d === 'L'){
		newX = newX + v;
	}else if(d === 'R'){
		newX = newX - v;
	}else if(d){
		//pan to country
		var e = getElem(d);
		if(e){
			var b = e.getBoundingClientRect(),
				offsetX = 0,
				offsetY = 0,
				CMidX = 0,
				CMidY = 0,
				amidX = 0,
				amidY = 0;

			console.log(e);
			console.log(b);
			console.log($svgMap['z']['now']);
			console.log(newX+' x '+newY);

			//if($LOC[d] && $LOC[d]['c']){
			//	console.log('OVERRIDE!');
				//country has override center position
			//	var $c = $LOC[d]['c'].split(',');
			//	CMidX = $c[0] * $svgMap['z']['now'];
			//	CMidY = $c[1] * $svgMap['z']['now'];
			
			//}else{
				//country center
				CMidX = b['left'] + (b['width'] / 2);
				CMidY = b['top'] + (b['height'] / 2);
			//}

			console.log('Country Center '+CMidX+' x '+CMidY);

			offsetX = Math.abs(CMidX);
			offsetY = Math.abs(CMidY);

			console.log('Country Offset '+offsetX+' x '+offsetY);
			
			newX = $svgMap['map']['x'] + offsetX;
			newY = $svgMap['map']['y'] + offsetX;

			console.log(newX+' x '+newY);
		}
	}


	newX = MapPanBoundX(newX);
	newY = MapPanBoundY(newY);

	if(newX !== $svgMap['map']['x']){
		$svgMap['map']['x'] = newX;
		mapinner.style.marginLeft = newX+'px';
	}
	if(newY !== $svgMap['map']['y']){
		$svgMap['map']['y'] = newY;
		mapinner.style.marginTop = newY+'px';
	}
}

function MapPanBoundX(v){
	var MinX = -Math.abs(($svgMap['box']['w'] * ($svgMap['z']['now'] - 1)) + (100 * $svgMap['z']['now']));
	if(v > 0){
		v = 0;
	}else if(v < MinX){
		v = MinX;
	}
	return v;
}

function MapPanBoundY(v){
	var MinY = -Math.abs($svgMap['box']['h'] * ($svgMap['z']['now'] - 1));
	if(v > 0){
		v = 0;
	}else if(v < MinY){
		v = MinY;
	}
	return v;
}

function MapPlotLatLng(lat, lng){
	var radius = $svgMap['map']['w'] / (2 * Math.PI),
		latRad = (lat * Math.PI) / 180,
		lonRad = ((parseFloat(lng) + 169.7) * Math.PI) / 180,
		x = lonRad * radius,
		vOffsetFromEquator = radius * Math.log(Math.tan(Math.PI / 4 + latRad / 2)),
		y = $svgMap['map']['h'] / 2 - vOffsetFromEquator;

	return {'x':x, 'y':y};
}

function MapAddPin(pin, $d){
	var xy = MapPlotLatLng($d['latitude'], $d['longitude']);
	console.log(xy);
	if(xy){
		var g = getElem('PointGroup');
		console.log(g);
		if(!g){
			var	s = getElem('svgMapInner').getElementsByTagName('svg')[0];
			g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
			g.setAttributeNS(null, 'id', 'PointGroup');
			s.appendChild(g);
		}
		var c = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
		c.setAttributeNS(null, 'cx', xy['x']);
		c.setAttributeNS(null, 'cy', xy['y']);
		c.setAttributeNS(null, 'r', 2);
		c.setAttributeNS(null, 'class', 'MapPoint');
		g.appendChild(c);
	}
}

function MapHiRes(v){
	var c = getElem(v),
		r = c.getAttribute('data-r');

	if(c && r){
		load_JSON($ENV['cdn']+'json/lib/svgMap/svgMap-mercator-hires-region-'+r+$ENV['min']+'.json'+$ENV['cache'], function(d){
			if(d){
				var dta = JSON.parse(d);
				if(dta && dta['land']){
					for(var x in dta['land']){
						var c = getElem(x);
						setAttr(c, {"d":dta['land'][x]['map']});
					}
				}
			}
		});
	}
}

var evt_map_wheel = function(f){
	var e = f.target,
		id = e.getAttribute('id');

	if(id === 'svgMapInner' || isDescendant(getElem('svgMapInner'), e) === true){
		clearTimeout(MapZoomWheelTimeout);
		var delta = ((f.deltaY || -f.wheelDelta || f.detail) >> 10) || 1;
		MapZoomWheel = MapZoomWheel + delta;
		MapZoomWheelTimeout = setTimeout(function(){
			MapZoom($svgMap['z']['now'] - MapZoomWheel, 'wheel');
			MapZoomWheel = 0;
		}, 150);
	}
}, evt_map_pointerup = function(f){
	var e = f.target,
		id = e.getAttribute('id');

	if(id === 'BtnZoomIn'){
		MapZoom($svgMap['z']['now'] + 1, '');
	}else if(id === 'BtnZoomOut'){
		MapZoom($svgMap['z']['now'] - 1, '');
	}
}, evt_map_pointermove = function(f){
	if(MapTooltip && !MapTooltip.classList.contains('hide')){
		MapTooltip.style.marginLeft = (mouseX - 73)+'px';
		MapTooltip.style.marginTop = (mouseY + 17)+'px';
	}
}, evt_map_resize = function(f){
	var maps = document.querySelectorAll('.svgMap'),
		len = maps.length;

	for(var i = 0; i < len; i++){
		svgMap_size(e[i]);
	}
	
};